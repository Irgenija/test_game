import random


class Player:
    """Base class"""

    def __init__(self, health):
        """Constructor"""

        self._initial_health = health
        self.health = health

    def damage(self, enemy, min_range, max_range):
        """Вealing damage"""

        added_damage = random.randint(min_range, max_range)

        if enemy.health > 0 and enemy.health > added_damage:
            enemy.health = enemy.health - added_damage
        else:
            enemy.health = 0
        return added_damage

    def min_damage(self, enemy):
        """Minimal damage"""

        damage_power = self.damage(enemy, 18, 25)
        print(f"The {self.enemy.__class__.__name__} took minimal damage "
              f"in an amount {damage_power}")

    def max_damage(self, enemy):
        """Maximal damage"""

        damage_power = self.damage(enemy, 10, 35)
        print(f"The {self.enemy.__class__.__name__} took maximal damage "
              f"in an amount {damage_power}")

    def healing(self):
        """Healing"""

        added_health = random.randint(18, 25)

        if self.health + added_health >= self._initial_health:
            self.health == self._initial_health
        else:
            self.health = self.health + added_health

        print(f"The {self.__class__.__name__} took healing "
              f"in an amount {added_health}")

    def action(self, enemy):
        """Action selection"""

        self.enemy = enemy
        marker = random.randint(1, 3)

        if marker == 1:
            self.min_damage(self.enemy)
        elif marker == 2:
            self.max_damage(self.enemy)
        elif marker == 3:
            self.healing()


class Computer(Player):
    def __init__(self, health):
        super().__init__(health)

        self.minimal_health = int(self._initial_health * 0.35)

    def action(self, enemy):
        """Increased chance of healing"""

        if self.health < self.minimal_health:
            self.enemy = enemy
            marker = random.randint(1, 3)

            if marker == 1:
                self.min_damage(self.enemy)
            elif marker == 2:
                marker_healing = random.randint(0, 1)

                if marker_healing == 0:
                    self.max_damage(self.enemy)
                elif marker_healing == 1:
                    self.healing()
            elif marker == 3:
                self.healing()
        else:
            self.enemy = enemy
            marker = random.randint(1, 3)

            if marker == 1:
                self.min_damage(self.enemy)
            elif marker == 2:
                self.max_damage(self.enemy)
            elif marker == 3:
                self.healing()


class Gamer(Player):
    pass


def start_action(player_1, player_2):
    """Game launch"""

    while player_1.health > 0 and player_2.health > 0:
        marker_action = random.randint(0, 1)

        if marker_action == 1:
            print("***Computer's action!***")
            player_1.action(player_2)
            print("Computer's health", player_1.health)
            print("Gamer's health", player_2.health, "\n")
        elif marker_action == 0:
            print("***Gamer's action!***")
            player_2.action(player_1)
            print("Computer's health", player_1.health)
            print("Gamer's health", player_2.health, "\n")
        else:
            print("Something went wrong!")

    if player_1.health == 0:
        print(player_2.__class__.__name__, "WIN!")
    else:
        print(player_1.__class__.__name__, "WIN!")


if __name__ == "__main__":
    computer = Computer(100)
    gamer = Gamer(100)
    start_action(computer, gamer)
