# Game simulator

### Basic requirements
- Python >= 3.6.7

## Setup
- Clone project
- Run game.py
```
python3 game.py
```